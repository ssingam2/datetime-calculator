import {
    ChronoUnit,
    LocalDate,
    LocalDateTime,
    LocalTime,
    Period,
} from "@js-joda/core";

export function daysBetween(
    start: LocalDate,
    end: LocalDate,
): number {
    return start.until(end, ChronoUnit.DAYS);
}

export function afterIntervalTimes(
    start: LocalDate,
    interval: Period,
    multiplier: number,
): LocalDate {
    return interval.multipliedBy(multiplier).addTo(start);
}

export function recurringEvent(
    start: LocalDateTime,
    end: LocalDateTime,
    interval: Period,
    timeOfDay: LocalTime,
): LocalDateTime[] {
    const resultArray: LocalDateTime[] = [];
    let localStart = start;
    while (localStart.isBefore(end) || localStart.isEqual(end)) {
        if (localStart.toLocalTime().isBefore(timeOfDay)) {
            localStart = timeOfDay.atDate(localStart.toLocalDate());
            resultArray.push(localStart);
        }
        localStart = interval.addTo(localStart.toLocalDate()).atTime(timeOfDay);
        resultArray.push(localStart);
        if (interval.addTo(localStart.toLocalDate()).atTime(timeOfDay)
            .isAfter(end))
            break;
    }
    return resultArray;
}