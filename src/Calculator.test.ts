import "jest-extended";

import {
    daysBetween,
    afterIntervalTimes,
    recurringEvent,
} from "./Calculator";

import {
    LocalDate,
    LocalDateTime,
    LocalTime,
    Period,
} from "@js-joda/core";

describe("daysBetween", () => {
    test("first provided example", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 31);
        const end: LocalDate = LocalDate.of(2022,1,31);
        expect(daysBetween(start, end)).toEqual(0);
    })
    test("Second provided example", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 30);
        const end: LocalDate = LocalDate.of(2022,1,31);
        expect(daysBetween(start, end)).toEqual(1);
    })
    test("Third provided example", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 31);
        const end: LocalDate = LocalDate.of(2022,1,30);
        expect(daysBetween(start, end)).toEqual(-1);
    })
    test("Last provided example", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 31);
        const end: LocalDate = LocalDate.of(2022,3,31);
        expect(daysBetween(start, end)).toEqual(59);
    })
})

describe("afterIntervalTimes", () => {
    test("Last provided example", () => {
        const start: LocalDate = LocalDate.of(2019, 1, 31);
        const interval : Period = Period.of(1, 1, 0);
        const multiplier : number = 1;
        expect(afterIntervalTimes(start, interval, multiplier)).toEqual(LocalDate.of(2020, 0o2, 29));
    })
    test("First provided example", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 31);
        const interval : Period = Period.of(0, 0, 1);
        const multiplier : number = 3;
        expect(afterIntervalTimes(start, interval, multiplier)).toEqual(LocalDate.of(2022, 0o2, 0o3));
    })    
    test("Second provided example", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 31);
        const interval : Period = Period.of(0, 0, 1);
        const multiplier : number = 0;
        expect(afterIntervalTimes(start, interval, multiplier)).toEqual(LocalDate.of(2022, 0o1, 31));
    }) 
    test("Third provided example", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 31);
        const interval : Period = Period.of(0, 1, 0);
        const multiplier : number = 1;
        expect(afterIntervalTimes(start, interval, multiplier)).toEqual(LocalDate.of(2022, 0o2, 28));
    }) 
});

describe("recurringEvent", () => {
    test("first provided example", () => {
        const start: LocalDateTime = LocalDateTime.parse('2022-01-01T00:00');
        const end: LocalDateTime = LocalDateTime.parse('2022-01-04T23:59');
        const interval : Period = Period.of(0, 0, 1);
        const timeOfDay : LocalTime = LocalTime.parse('01:00');
        const expectedDates = [
            LocalDateTime.parse('2022-01-01T01:00'),
            LocalDateTime.parse('2022-01-02T01:00'),
            LocalDateTime.parse('2022-01-03T01:00'),
            LocalDateTime.parse('2022-01-04T01:00'),
          ];
        expect(recurringEvent(start, end, interval, timeOfDay)).toEqual(expectedDates);
    })
    test("Second provided example", () => {
        const start: LocalDateTime = LocalDateTime.parse('2022-01-01T02:00');
        const end: LocalDateTime = LocalDateTime.parse('2022-01-04T23:59');
        const interval : Period = Period.of(0, 0, 1);
        const timeOfDay : LocalTime = LocalTime.parse('01:00');
        const expectedDates = [
            LocalDateTime.parse('2022-01-02T01:00'),
            LocalDateTime.parse('2022-01-03T01:00'),
            LocalDateTime.parse('2022-01-04T01:00'),
          ];
        expect(recurringEvent(start, end, interval, timeOfDay)).toEqual(expectedDates);
    })
    test("third provided example", () => {
        const start: LocalDateTime = LocalDateTime.parse('2022-01-01T00:00');
        const end: LocalDateTime = LocalDateTime.parse('2022-01-04T00:00');
        const interval : Period = Period.of(0, 0, 1);
        const timeOfDay : LocalTime = LocalTime.parse('01:00');
        const expectedDates = [
            LocalDateTime.parse('2022-01-01T01:00'),
            LocalDateTime.parse('2022-01-02T01:00'),
            LocalDateTime.parse('2022-01-03T01:00'),
          ];
        expect(recurringEvent(start, end, interval, timeOfDay)).toEqual(expectedDates);
    })
    test("Last provided example", () => {
        const start: LocalDateTime = LocalDateTime.parse('2022-01-31T00:00');
        const end: LocalDateTime = LocalDateTime.parse('2022-05-15T00:00');
        const interval : Period = Period.of(0, 1, 0);
        const timeOfDay : LocalTime = LocalTime.parse('01:00');
        const expectedDates = [
            LocalDateTime.parse('2022-01-31T01:00'),
            LocalDateTime.parse('2022-02-28T01:00'),
            LocalDateTime.parse('2022-03-28T01:00'),
            LocalDateTime.parse('2022-04-28T01:00'),
          ];
        expect(recurringEvent(start, end, interval, timeOfDay)).toEqual(expectedDates);
    })

});